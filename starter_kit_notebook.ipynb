{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0b2bb01c",
   "metadata": {},
   "source": [
    "# OPS-SAT case starter-kit notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b9611e0",
   "metadata": {},
   "source": [
    "ESA's [Kelvins](https://kelvins.esa.int) competition \"[the OPS-SAT case](https://kelvins.esa.int/opssat/home/)\" is a novel data-centric challenge that asks you to work with the raw data of a satellite and very few provided labels to find the best parameters for a given machine learning model. Compared to previous competitions on Kelvins (like the [Pose Estimation](https://kelvins.esa.int/pose-estimation-2021/) or the [Proba-V Super-resolution challenge](https://kelvins.esa.int/proba-v-super-resolution/)) where the test-set is provided and the infered results are submitted, for the OPS-SAT case, we will run inference on the Kelvins server directly! This notebooks contains examples on how you can load your data and train an **EfficientNetLite0** model by only using the 80-labeled images provided. Therefore, the directory `images`, containing unlabeld patches and included in the training dataset is not used for this notebook. However, competitors are encouraged to use these patches to improve the model accuracy."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2993066d",
   "metadata": {},
   "source": [
    "# 1. Module imports"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "039fe190",
   "metadata": {},
   "source": [
    "If you do not have a GPU, uncomment and run the next commands.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d7b9414",
   "metadata": {},
   "outputs": [],
   "source": [
    "#import os\n",
    "#os.environ[\"CUDA_VISIBLE_DEVICES\"]=\"-1\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1354dcd3",
   "metadata": {},
   "source": [
    "Other imports."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a045946a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "from tensorflow import keras\n",
    "\n",
    "import numpy as np\n",
    "from sklearn.metrics import cohen_kappa_score\n",
    "\n",
    "from efficientnet_lite import EfficientNetLiteB0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14c369e7",
   "metadata": {},
   "source": [
    "# 2. Utility Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0ce95a9",
   "metadata": {},
   "source": [
    "You can use this function to load your training data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b93e7152",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_images_from_path(dataset_path):\n",
    "    \"\"\" Get images from path and normalize them applying channel-level normalization. \"\"\"\n",
    "\n",
    "    # loading all images in one large batch\n",
    "    tf_eval_data = tf.keras.utils.image_dataset_from_directory(dataset_path, image_size=input_shape[:2], shuffle=False, \n",
    "                                                               batch_size=100000)\n",
    "\n",
    "    # extract images and targets\n",
    "    for tf_eval_images, tf_eval_targets in tf_eval_data:\n",
    "        break\n",
    "\n",
    "    return tf.convert_to_tensor(tf_eval_images), tf_eval_targets"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "732e397f",
   "metadata": {},
   "source": [
    "# 3. Loading the model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03702ebb",
   "metadata": {},
   "source": [
    "The network architecture used for OPS-SAT is **EfficientNetLite0**. We would like to thank Sebastian for making a Keras implementation of EfficientNetLite publicly available under the Apache 2.0 License: https://github.com/sebastian-sz/efficientnet-lite-keras. Our Version of this code has been modified to better fit our purposes. For example, we removed the ReLU \"stem_activation\" to better match a related efficientnet pytorch implementation. In any way, **you have to use the model architecture that we provide in our [starter-kit](https://gitlab.com/EuropeanSpaceAgency/the_opssat_case_starter_kit).**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d85d1d4",
   "metadata": {},
   "outputs": [],
   "source": [
    "input_shape = (200, 200, 3)   # input_shape is (height, width, number of channels) for images\n",
    "num_classes = 8\n",
    "model = EfficientNetLiteB0(classes=num_classes, weights=None, input_shape=input_shape, classifier_activation=None)\n",
    "model.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c318836",
   "metadata": {},
   "source": [
    "# 4. Loading data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db2b233b",
   "metadata": {},
   "source": [
    "Uncomment next line and adjust with the path of your dataset. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a5a885c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "#dataset_path=\"path to your dataset.\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f568413",
   "metadata": {},
   "source": [
    "In this notebook, classical supervised learning is used. Therefore, remember to remove the subdirectory `images` containing unlabeled patches before loading the dataset to perform training correctly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c99d3d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Loading dataset\n",
    "x_train, y_train=get_images_from_path(dataset_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b368a33",
   "metadata": {},
   "source": [
    "# 5. Model training"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72364046",
   "metadata": {},
   "source": [
    "We provide now an example on how you can train your model by using standard supervised learning. Training loss (`SparseCategoricalCrossentropy`) and `Accuracy` are shown for simplicity and for an easier interpretation of the training outcome, despite your submission will be evaluated by using the metric **1 - Cohen's kappa** [metric](https://en.wikipedia.org/wiki/Cohen's_kappa). For more information on scoring, please refer to [Scoring](https://kelvins.esa.int/opssat/scoring/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac1cb6dd",
   "metadata": {},
   "outputs": [],
   "source": [
    "model.compile(optimizer='adam',\n",
    "              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), metrics=[keras.metrics.SparseCategoricalAccuracy()])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cbc20e92",
   "metadata": {},
   "source": [
    "With this model and the dataset provided, please do your best!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "79f6638e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# load data, data augmentation, training, overfitting, transfer-learning etc.\n",
    "history=model.fit(x_train, y_train, epochs=55, verbose=1, batch_size=8)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74b45836",
   "metadata": {},
   "source": [
    "Calculating the **1 - Cohen's kappa** score of the trained model on the trained dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d705a169",
   "metadata": {},
   "outputs": [],
   "source": [
    "predictions = np.zeros(len(y_train), dtype=np.int8)\n",
    "\n",
    "# inference loop\n",
    "for e, (image, target) in enumerate(zip(x_train, y_train)):\n",
    "    image = np.expand_dims(np.array(image), axis=0)\n",
    "    output = model.predict(image)\n",
    "    predictions[e] = np.squeeze(output).argmax()\n",
    "\n",
    "#Keras model score\n",
    "score_keras = 1 - cohen_kappa_score(y_train.numpy(), predictions)\n",
    "print(\"Score:\",score_keras)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2a89c8b",
   "metadata": {},
   "source": [
    "# 6. Saving and loading trained model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "012ec307",
   "metadata": {},
   "source": [
    "The trained model can be now saved by using HDF5-format that is the only accepted for submission. The name `test.h5` will be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7170861",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Saving model\n",
    "model.save_weights('test.h5')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3625f91",
   "metadata": {},
   "source": [
    "The trained model can be also loaded for further testing. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2ff1463",
   "metadata": {},
   "outputs": [],
   "source": [
    "model = EfficientNetLiteB0(classes=num_classes, weights=None, input_shape=input_shape, classifier_activation=None)\n",
    "model.load_weights('test.h5')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bbabf34d",
   "metadata": {},
   "source": [
    "The model will be now compiled and tested again. You should get the same score as before saving and loading. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1e583c84",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Model shall be compiled before testing.\n",
    "model.compile()\n",
    "\n",
    "#Creating empty predictions\n",
    "predictions = np.zeros(len(y_train), dtype=np.int8)\n",
    "\n",
    "# inference loop\n",
    "for e, (image, target) in enumerate(zip(x_train, y_train)):\n",
    "    image = np.expand_dims(np.array(image), axis=0)\n",
    "    output = model.predict(image)\n",
    "    predictions[e] = np.squeeze(output).argmax()\n",
    "\n",
    "#Keras model score\n",
    "score_keras = 1 - cohen_kappa_score(y_train.numpy(), predictions)\n",
    "print(\"Score:\",score_keras)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
